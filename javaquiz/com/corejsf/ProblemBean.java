package com.corejsf;

//package src.java.com.corejsf;

import java.io.Serializable;
import java.util.ArrayList;

//@Named
//@SessionScoped
public class ProblemBean implements Serializable {

  private String question, correctAnswer, categoryName;
  private int correctAnswerIndex;
  private String[] answers;


//    public ProblemBean(String categoryName, String firstQuestion, ArrayList<String> choices, String correctAnswer) {

  public ProblemBean() {
      
  }
  
  public ProblemBean(String categoryName, String newQuestion, String[] answers, int cAnswerIndex) {
      this.categoryName = categoryName;
      this.question = newQuestion;
      this.answers = answers;
      this.correctAnswerIndex = cAnswerIndex; 
      this.correctAnswer = answers[correctAnswerIndex];
  }
  

  public int getCorrectAnswerIndex() {
      return this.correctAnswerIndex;
  }
  
  public String getCorrectAnswer() {
      return answers[getCorrectAnswerIndex()];
  }
  
  public void setCorrectAnswerIndex(int num) {
      this.correctAnswerIndex = num;
  }
  public String getCategoryName() {
      return categoryName;
  }
  
  public void setQuestion(String newQuestion) {
      this.question = newQuestion;
  }
  
  public String getQuestion() {
      return this.question;
  }
  
  public void setCategoryName(String name) {
      this.categoryName = name;
  }
  
  public void setCorrectAnswer(String cAnswer) {
      this.correctAnswer = cAnswer; 
  }
  
//  public String getCorrectAnswer() {
//      return this.correctAnswer;
//  }

  public ArrayList<String> getAnswers() {
//  public String[] getAnswers() {
  //public String[] getAnswers() {
      ArrayList<String> stringList = new ArrayList<>();
      for (int i = 0; i < 4; ++i) {
          stringList.add(answers[i]);
      }
      //return this.answers;
      return stringList;
  }
  
  public void setAnswers(String[] answerArray) {
      for (int i = 0; i < 4; ++i) {
          answers[i] = answerArray[i];
      }
  }

  // override for more sophisticated checking
  public boolean isCorrect(String response) {
      int responseIndex;
      for (int i = 0; i < 4; ++i) {
          if (answers[i].toLowerCase().contentEquals(response.toLowerCase())) {
              responseIndex = i;
              if (correctAnswerIndex == responseIndex) {
                  return true;
              } else {
                  return false;
              }
          }
      } return false;
    //return response.trim().equalsIgnoreCase(correctAnswe.trim().);
  }

}
