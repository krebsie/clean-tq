package com.corejsf;

//package src.java.com.corejsf

//package com.corejsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class QuizBean implements Serializable {

  private int currentIndex;
  private int tries;
  private int score, numQuestions;
  private String response = "";
  private String correctAnswer, currentCategory, currentQuestion, lastCorrectAnswer;
  private ArrayList<ProblemBean> firstProblems = new ArrayList<>();
  private Map<String, List<ProblemBean>> byCategory = new HashMap<>();
  private ProblemBean currentProblemBean, firstProblemBean;
  private boolean gotLastQuestionRight, outOfQuestions;
  private DatabaseBean db;
  private ArrayList<ProblemBean> problemList = new ArrayList<>();
  private ArrayList<String> answers = new ArrayList<>();
  
  

  // Here, we hardwire the firstProblems. In a real application,
  // they would come from a database
  //(String categoryName, String question, String[] choices, String correctAnswer)
  //

  public QuizBean() {
      firstProblems.add(0, new ProblemBean("Computer Science", "If you need to sort a"
              + " very large list of integers (billions), what efficient sorting "
              + "algorithm is your best bet?", new String[]{"Quicksort", "Mergesort", "Radix Sort", "Bubblesort"}, 0));
      firstProblems.add(1, new ProblemBean("Mathematics", "What does pi equal?",  new String[]{"3.15490", "3.14679", "3.14159", "3.14169"}, 2));
      firstProblems.add(2, new ProblemBean("Physics", "What is equal to the product of mass and acceleration?", new String[]{"Force", "Momentum", "Density", "Volume"}, 0));
      firstProblems.add(3, new ProblemBean("Astronomy", "What is the name of the most prominent crater on the Moon?", new String[]{"Tycho", "Copernicus", "Kepler", "Galileo"}, 0));
      tries = 0;
      score = 0;
      outOfQuestions = false;
      db = new DatabaseBean();
      //byCategory = firstProblems.stream().collect(Collectors.groupingBy(ProblemBean::getQuestion));
    }
  
    public void setCurrentQuestion(String question) {
      this.currentQuestion = question;
    }
    
      public String getCurrentQuestion() {
      if (currentIndex >= problemList.size()) {
          System.err.println("error in getcurquestion");
          return null;
      } else {
        return currentProblemBean.getQuestion();
      }
  }
 
    public String beginAction() {

      switch(currentCategory) {
          case "Computer Science":
              setCurrentCategory("Computer Science");
              ++numQuestions;
              processProblemBeans("Computer Science");
              break;
          case "Physics":
              setCurrentCategory("Physics");
              ++numQuestions;
              processProblemBeans("Physics");              
              break;
          case "Mathematics":
              setCurrentCategory("Mathematics");
              processProblemBeans("Mathematics");              
              break;
          case "Astronomy":
              setCurrentCategory("Astronomy");
              ++numQuestions;
              processProblemBeans("Astronomy");
              break;
      }
      return "first";
  }
  
    public void processProblemBeans(String categoryName) {
      db.getProblems(categoryName).stream().forEach((entry) -> {
          problemList.add(entry);
      });
      setCurrentIndex(0);
      setCurrentProblemBean(currentIndex);
      setCurrentQuestion(currentProblemBean.getQuestion());     
      tries = 0;
      setScore(0);
    }
   
    public String checkAnswerAction() {
    tries++;
    if (problemList.get(currentIndex).isCorrect(response)) {
        setScore(score + 1);
        setGotLastQuestionRight(true);
        if ((getOutOfQuestions() == true) || ((problemList.size() - 1) == currentIndex)) {
            return "done";
        } else {
            nextProblem();
            return "rest";
        }
    } else {
        setGotLastQuestionRight(false);
        setLastCorrectAnswer(correctAnswer);
        if ((getOutOfQuestions() == true) || ((problemList.size() - 1) == currentIndex)) {
            return "done";
        } else {
            nextProblem();
            return "rest";
        }
    }
  }

    public String endAction() {
        setLastCorrectAnswer(lastCorrectAnswer);
        setCurrentIndex(0);
        tries = 0;
        response = "";
        numQuestions = 0;
            //Collections.shuffle(firstProblems);
        setOutOfQuestions(false);
        currentIndex = 0;
        problemList.clear();
        answers.clear();
    //return "startOver";
        return "index";
    }

  private void nextProblem() {
      answers.clear();
      ++numQuestions;
    setLastCorrectAnswer(currentProblemBean.getCorrectAnswer());
    ++currentIndex;
    response = "";
    if (!(currentIndex == problemList.size() - 1)) {
       setCurrentProblemBean(currentIndex);
    }
    setCurrentProblemBean(currentIndex);
    setCurrentQuestion(currentProblemBean.getQuestion());
  }
  
  public ArrayList<String> getAnswers() {
      for (int i = 0; i < 4; ++i){
          answers.add(i, currentProblemBean.getAnswers().get(i));
      }
      return answers;
  }
  
  public void setAnswers(ArrayList<String> newAnswers) {
      for (int i=0; i < 4; ++i) {
          answers.add(i, newAnswers.get(i));
      }
  }
  
  public int getNumQuestions() {
      return this.numQuestions;
  }
  
  public void setNumQuestions(int num) {
      this.numQuestions = num;
  }

  public String getCorrectAnswer() {
      if (currentIndex >= problemList.size()) {
          System.err.println("error in getcorrectanswer");
          return null;
      } else {
        return problemList.get(currentIndex).getCorrectAnswer();
      }
  }

  public String getResponse() {
    return response;
  }

  public void setResponse(String newValue) {
    response = newValue;
  }
  
  public void setCurrentCategory(String newCategory) {
      this.currentCategory = newCategory;
  }
  
  public String getCurrentCategory() {
      return this.currentCategory;
  }
  
  public void setScore(int nScore) {
      this.score = nScore;
  }
  
    public int getScore() {
    return score;
  }
  
  public void setGotLastQuestionRight(boolean val) {
      this.gotLastQuestionRight = val;
  }
  
  public boolean getGotLastQuestionRight() {
      return this.gotLastQuestionRight;
  }
  
  public void setCurrentIndex(int number) {
      this.currentIndex = number;
  }
  
  public int getCurrentIndex() {
      return this.currentIndex;
  }
  
  public void setFirstProblemBean(int problemNum) {
      this.firstProblemBean = firstProblems.get(problemNum);
      setCurrentProblemBean(firstProblemBean);
  }
  
    public ProblemBean getFirstProblemBean() {
        if (currentIndex >= firstProblems.size()) {
          System.err.println("problem in getCurrentProblem");
          return null;
      } else {
        return this.firstProblems.get(currentIndex);
      }
  }
  public void setCurrentProblemBean(int problemNum) {
      //this.currentProblemBean = firstProblems.get(problemNum);
      this.currentProblemBean = problemList.get(problemNum);
  }
  
  public void setCurrentProblemBean(ProblemBean newP) {
      this.currentProblemBean = newP;
  }
  
    public ProblemBean getCurrentProblemBean() {
      if (currentIndex >= problemList.size()) {
          System.err.println("problem in getCurrentProblem");
          return null;
      } else {
          if (problemList.isEmpty()) {
              return this.firstProblems.get(currentIndex);
          } else {
            return this.problemList.get(currentIndex);
          }
      }
  }

  public void setOutOfQuestions(boolean out) {
      this.outOfQuestions = out;
  }
  
  public boolean getOutOfQuestions() {
      return this.outOfQuestions;
  }
  
  public String getLastCorrectAnswer() {
      return this.lastCorrectAnswer;
  }
  
  public void setLastCorrectAnswer(String ans) {
      this.lastCorrectAnswer = ans;
  }
}