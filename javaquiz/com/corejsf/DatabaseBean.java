package com.corejsf;

/*
 * Adjust the package name to match your top-level package.
 */
//package src.java.com.corejsf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * The database bean provides access to the database.
 * The current implementation is just a stand-in for a future database.
 * The future database bean will have the same methods plus a few more when
 * the apllication is enhanced to keep track of scores for different users.
 * 
 * Beans that invoke database bean methods should declare a database variable as
 * follows:
 * 
 * @Inject private Database database;
 * 
 */
@Named(value = "database")
@ApplicationScoped
public class DatabaseBean {

  private Map<String, List<ProblemBean>> problems;

  /**
   * addProblem(problem) adds problem to the list of problems for the category
   * named problem.categoryName.
   * @param problem the problem to be added
   */
  private void addProblem(ProblemBean problem) {
    if (problems == null) {
      problems = new HashMap<String, List<ProblemBean>>();
    }
    String categoryName = problem.getCategoryName();
    List<ProblemBean> category = problems.get(categoryName);
    if (category == null) {
      category = new ArrayList<ProblemBean>();
      problems.put(categoryName, category);
    }
    category.add(problem);
  }

  /**
   * Creates the database bean.
   */
  public DatabaseBean() {
    // Add your own trivia quiz problems here by invoking addProblem().
    // The argument for invokation should be an invokation of a Problem
    // constructor like the following:
//    addProblem(new ProblemBean("Physics",
//        "What is the product of mass and acceleration?",
//        new String[]{"force", "momentum", "density", "volume"},
//        0));
    addProblem(new ProblemBean("Computer Science", "If you need to sort a"
              + " very large list of integers (billions), what efficient sorting "
              + "algorithm is your best bet?", new String[]{"Quicksort", "Mergesort", "Radix Sort", "Bubblesort"}, 0));
      addProblem(new ProblemBean("Mathematics", "What does pi equal?",  new String[]{"3.15490", "3.14679", "3.14159", "3.14169"}, 2));
      addProblem(new ProblemBean("Physics", "What is equal to the product of mass and acceleration?", new String[]{"Force", "Momentum", "Density", "Volume"}, 0));
      addProblem(new ProblemBean("Astronomy", "What is the name of the most prominent crater on the Moon?", new String[]{"Tycho", "Copernicus", "Kepler", "Galileo"}, 0));
      addProblem(new ProblemBean("Computer Science", "Which of the following is a means of compressing images by blurring the boundaries between different colors while maintaining all brightness information?", new String[]{"JPEG", "LZW", "MIDI", "GIF"}, 0));
      addProblem(new ProblemBean("Computer Science", "Which of the following is the binary representation of 4 5/8?", new String[]{"110.101", "100.101", "10.011", "100.11"}, 1));
      addProblem(new ProblemBean("Computer Science", "Which of the following is a major standardization organization within the U.S.?", new String[]{"LZW", "ISO", "ANSI", "ASCII"}, 2));
      addProblem(new ProblemBean("Computer Science", "When users are involved in complex tasks, the demand on __________ can be significant.", new String[]{"Short-term memory", "Shortcuts", "Long-term memory", "Their ability to focus"}, 0));
      addProblem(new ProblemBean("Astronomy", "When there are two full moons in the same calendar month, the second is called a \"Blue Moon\". How often does this happen on average? Every ________________ years.", new String[]{"1 1/2 to 2", "2 1/2 to 3", "3 1/2 to 5", "5 1/2 to 10"}, 1));
      addProblem(new ProblemBean("Astronomy", "What color is on the outside of the arc of a rainbow?", new String[]{"White", "Violet", "Green", "Red"}, 3));
  }

  /**
   * database.getProblems(categoryName) returns the list of problems in the
   * database whose category name is categoryName.
   * @param categoryName
   * @return a list of problems
   */
  public List<ProblemBean> getProblems(String categoryName) {
    return problems.get(categoryName);
  }

  /**
   * database.getCategoryNames() returns the list of category names in the
   * database.
   * @return the list of categoryNames
   */
  public List<String> getCategoryNames() {
    return Arrays.asList(problems.keySet().toArray(new String[0]));
  }

}